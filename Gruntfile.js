module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    shell: {
      options : {
        stdout: true
      },
      npm_install: {
        command: 'npm install'
      },
      bower_install: {
        command: './node_modules/.bin/bower install'
      }
    },

    project: {
      // configurable paths
      app: 'app',
      dist: 'dist'
    },
    
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          // target.css file: source.less file
          "styles/bootstrap.css": "bower_components/bootstrap/less/bootstrap.less",
          "styles/css/*.css": "styles/less/*.less"
        }
      }
    },
    watch: {
      styles: {
        files: ['less/**/*.less'], // which files to watch
        tasks: ['less'],
        options: {
          nospawn: true
        }
      }
    },

    bowerInstall: {
      target: {
        src: ['<%= project.app %>/index.html']
      }
    },

    

    connect: {
      server: {
        options: {
          port: 9001,
          base: 'app/'
        }
      },
      testserver: {
        options: {
          port: 9999,
          base: 'app/'
        }
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/<%= pkg.name %>.js',
        dest: 'build/<%= pkg.name %>.min.js'
      }
    },

    karma: {
      unit: {
        configFile: './test/karma-unit.conf.js',
        singleRun: false
      },
      e2e: {
        configFile: './test/karma-e2e.conf.js',
        singleRun:true
      }
    }


  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-bower-install');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-shell');

  // Default task(s).
  grunt.registerTask('install', ['shell:npm_install','shell:bower_install','uglify','less','bowerInstall']);
  grunt.registerTask('dev', ['uglify','less','bowerInstall','connect:server','watch']);
  grunt.registerTask('build', ['uglify','less','bowerInstall']);
  grunt.registerTask('test', ['connect:testserver','karma:e2e']);
  grunt.registerTask('test:e2e', ['connect:testserver','karma:e2e']);
  grunt.registerTask('test:unit', ['connect:testserver','karma:unit']);
  grunt.registerTask('citest', ['install','connect:testserver','karma:e2e']);
  grunt.registerTask('serve', ['connect:webserver']);

};
