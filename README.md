# README #

This is just an angularJS Skeleton using some extra tools.
 
* Grunt
* Bower
* PhantomJS
* Karma
* Bootstrap 3
* AngularJS


### What is this repository for? ###

* Version 0.1.0
* Testing tools

### How do I get set up? ###

* First of all to make this work we need to install nodejs, and npm. Make sure you can run npm and install things without the need of sudo.
* Now install grunt and bower with
    
```
#!javascript

npm install -g grunt-cli
npm install -g bower
grunt install
```

* Run grunt commands configured in Gruntfile
* * grunt dev : in case you want to use your application. It will start at localhost:9001
* * grunt install: in case you want to install a new dependency added to package.json or bower.json
* * grunt test: in case you want to test (there is also test:unit and test:e2e)
* * grunt serve: in case you just want to get your server running


### Any questions or suggestions? ###

* Ricardo Hernández -> ricardo@technogi.com.mx