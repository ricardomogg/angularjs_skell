describe('E2E Test Hello World!', function(){
    beforeEach(function() {
    browser().navigateTo('/index.html');
  });

  it('should say Hello World!', function() {
    browser().navigateTo('#/');
    expect(element('#hello_world').html()).toContain('Hello World!');
  });
});