// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function() {
  return {
    
    basePath: '../',

    files: [
      'app/bower_components/jquery/dist/jquery.js'
    ],

    exclude: [],
    autoWatch: true,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['PhantomJS']
    
  }
};