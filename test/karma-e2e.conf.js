var sharedConfig = require('./karma-shared.conf');

module.exports = function(config){
  var conf = sharedConfig();

  conf.frameworks = ['ng-scenario','jasmine'];

  conf.files = conf.files.concat([
    'test/e2e/**/*.js'
  ]);

  conf.proxies = {
    '/': 'http://localhost:9999/'
  };

  conf.reporters= ['progress', 'junit'];

  conf.junitReporter = {
    outputFile: 'test/results/e2e-test-results.xml',
    suite: ''
  };


  config.set(conf);
};